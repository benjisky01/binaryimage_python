import cv2
import numpy as np 
from matplotlib import pyplot as plt

img = cv2.imread('deadpool.jpg',2)
ret,img1 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
ret,img2 = cv2.threshold(img,127,255,cv2.THRESH_BINARY_INV)

titres =['Image original','Image Binaire','Image Binaire Inversée']
images = [img,img1,img2]

for i in range(3):
    plt.subplot(2,2,i+1),plt.imshow(images[i],'gray')
    plt.title(titres[i])
    plt.xticks([]),plt.yticks([])


plt.show()
print(img)

